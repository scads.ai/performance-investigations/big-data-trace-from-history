#!/usr/bin/env python
import argparse
import pathlib
import os
import sys
import requests
from datetime import datetime
epoque = datetime.utcfromtimestamp(0)
baseUrlSuffix = "/jobs"

def toMillies(timestr):
    return int(timestr)

def getAppsSorted(baseUrl):
    response = requests.get(baseUrl+"/overview")
    appIds = []
    if response.ok:
        apps = response.json()["jobs"]
        sortedApps = sorted(apps, key=lambda app: app["end-time"], reverse=True)
        for app in sortedApps:
            appIds.append(app["jid"])
    return appIds

def prepareData(baseUrl, app):
    appUrl = baseUrl + "/" + app
    verticeList = []
    response = requests.get(appUrl)
    if response.ok:
        vertices = response.json()["vertices"]
        sortedVertices = sorted(vertices, key=lambda vertice: vertice["start-time"])
    if response.ok:
        verticeTaskId = 0
        for vertice in sortedVertices:
            verticeId = vertice["id"]
            verticeData = {
                "name": "Vertice %s: %s"%(str(verticeTaskId), vertice["name"]),
                "startTime": toMillies(vertice["start-time"]),
                "endTime": toMillies(vertice["end-time"]),
                "read-bytes": vertice["metrics"]["read-bytes"],
                "read-records": vertice["metrics"]["read-records"],
                "write-bytes": vertice["metrics"]["write-bytes"],
                "write-records": vertice["metrics"]["write-records"],
                "taskData": []
            }
            taskTimesResponse = requests.get(appUrl + "/vertices/" + verticeId + "/subtasktimes")
            taskMetricsResponse = requests.get(appUrl + "/vertices/" + verticeId)
            if taskTimesResponse.ok and taskMetricsResponse.ok:
                tasks = taskTimesResponse.json()["subtasks"]
                taskMetrics = taskMetricsResponse.json()["subtasks"]
                sortedTasks = sorted(tasks, key=lambda task: task["subtask"])
                sortedTaskMetrics = sorted(taskMetrics, key=lambda task: task["subtask"])
                for i in range(len(sortedTasks)):
                    timestamps = sortedTasks[i]["timestamps"]
                    taskMetric = sortedTaskMetrics[i]["metrics"]
                    status = sortedTaskMetrics[i]["status"]
                    stoppedTime = timestamps["FINISHED"]
                    if status == "CANCELED":
                        stoppedTime = timestamps["CANCELED"]
                    taskData = {
                        "name": "Vertice %s: Task %s" % (str(verticeTaskId),str(i)),
                        "CREATED": toMillies(timestamps["CREATED"]),
                        "SCHEDULED": toMillies(timestamps["SCHEDULED"]),
                        "DEPLOYING": toMillies(timestamps["DEPLOYING"]),
                        "INITIALIZING": toMillies(timestamps["INITIALIZING"]),
                        "RUNNING": toMillies(timestamps["RUNNING"]),
                        "STOPPED": toMillies(stoppedTime),
                        "read-bytes": taskMetric["read-bytes"],
                        "read-records": taskMetric["read-records"],
                        "write-bytes": taskMetric["write-bytes"],
                        "write-records": taskMetric["write-records"],
                    }
                    verticeData["taskData"].append(taskData)
            verticeList.append(verticeData)
            verticeTaskId += 1
    return verticeList

def createTraceFile(preparedData, appFolder):
    with otf2.writer.open(appFolder, timer_resolution=1000) as trace:

        parent_node = trace.definitions.system_tree_node("node")
        system_tree_node = trace.definitions.system_tree_node("saurus1", parent=parent_node)
        inputAttributeNames = ["read-bytes", "read-records"]
        outputAttributeNames = ["write-bytes", "write-records"]
        inputAttributeDefs = list(map(lambda x: trace.definitions.attribute(x, x, Type.UINT64), inputAttributeNames))
        outputAttributeDefs = list(map(lambda x: trace.definitions.attribute(x, x, Type.UINT64), outputAttributeNames))

        for stage in preparedData:
            location_group = trace.definitions.location_group(stage["name"], system_tree_parent=system_tree_node)
            writer = trace.event_writer(stage["name"], group=location_group)
            function = trace.definitions.region("Vertice")
            inputAttributes = {inputAttributeDefs[i]: stage[inputAttributeNames[i]] for i in range(len(inputAttributeNames))}
            outputAttributes = {outputAttributeDefs[i]: stage[outputAttributeNames[i]] for i in range(len(outputAttributeNames))}
            writer.enter(stage["startTime"], function, inputAttributes)
            writer.leave(stage["endTime"], function, outputAttributes)
            for task in stage["taskData"]:
                writer = trace.event_writer(task["name"], group=location_group)
                timeIndexes = ["CREATED", "SCHEDULED", "DEPLOYING", "INITIALIZING", "RUNNING", "STOPPED"]
                for i in range(len(timeIndexes) - 1):
                    function = trace.definitions.region(timeIndexes[i])
                    if "RUNNING" == timeIndexes[i]:
                        inputTaskAttributes = {inputAttributeDefs[i]: task[inputAttributeNames[i]] for i in range(len(inputAttributeNames))}
                        outputTaskAttributes = {outputAttributeDefs[i]: task[outputAttributeNames[i]] for i in range(len(outputAttributeNames))}
                        writer.enter(task[timeIndexes[i]], function, inputTaskAttributes)
                        writer.leave(task[timeIndexes[i + 1]], function, outputTaskAttributes)
                    else:
                        writer.enter(task[timeIndexes[i]], function)
                        writer.leave(task[timeIndexes[i + 1]], function)

def createFunctionGroupFile(outputFolder):
    path=pathlib.Path(outputFolder) / "function-groups.def"
    with path.open(mode="w") as f:
        f.write("""BEGIN_OPTIONS
    MATCHING_STRATEGY=FIRST
    CASE_SENSITIVE_FUNCTION_NAME=NO
    CASE_SENSITIVE_MANGLED_NAME=NO
    CASE_SENSITIVE_SOURCE_FILE_NAME=NO
END_OPTIONS

BEGIN_FUNCTION_GROUP CREATED
    NAME=*CREATED*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP SCHEDULED
    NAME=*SCHEDULED*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP DEPLOYING
    NAME=*DEPLOYING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP INITIALIZING
    NAME=*INITIALIZING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP RUNNING
    NAME=*RUNNING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP Vertice
    NAME=*
END_FUNCTION_GROUP
""")

parser = argparse.ArgumentParser(description="Create trace from job history")
parser.add_argument('--output-folder', dest="outputFolder", type=pathlib.Path, default=os.environ["HOME"]+"/log-data-trace")
parser.add_argument('--scorep-install-path', dest="scorepInstallPath", type=pathlib.Path, default="/scratch/ws/1/s2817051-easybuild-test/software/Score-P/8.0-dev-java-gompic-2020b")
parser.add_argument('--base-url', dest="baseUrl", default="http://localhost:8080")
parser.add_argument('--only-latest', dest="latestOnly", action="store_true")
parser.set_defaults(latestOnly=False)


args = parser.parse_args()
baseUrl = args.baseUrl + baseUrlSuffix
#before importing otf2, the path to it has to be configured
#take the first path matching <scorep_install_path>/lib/python*/site-packages:
sys.path.append(str(next(args.scorepInstallPath.glob("lib/python*/site-packages"))))
import otf2
from otf2.enums import Type
outputFolder = args.outputFolder
if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)
apps = getAppsSorted(baseUrl)
if args.latestOnly:
    if len(apps) > 0:
        apps = [apps[0]]
    else:
        apps = []
for app in apps:
    appFolder = outputFolder / app
    preparedData = prepareData(baseUrl, app)
    createTraceFile(preparedData, str(appFolder))
    createFunctionGroupFile(appFolder)
