# Scripts to fetch events from the Spark or Flink history server and create a trace in OTF2 (Open Trace Format 2)

## What the scripts do

The general workflow is as following:
1. The history server is asked for a list of Spark/Flink applications.
2. For each application, a trace is created containing stage and task start and end events.
3. Vampir can be used to view traces.

## Requirements

The scripts expect a Spark or Flink history server, further processes are not required. Additionally, a Score-P installation and Python 3 is required.

## Running the scripts to create traces

You can find a Python 3 script for fetching data from the history server in history-spark.py and history-flink.py, respectively. Corresponding Bash scripts can be found in run.sh and run-flink.sh. You need to configure the variables before running `run.sh` or `run-flink.sh` to avoid usage of (probably wrong) default values:

```bash
OUTPUT_DIR=$PWD
SCOREP_INSTALL=$PWD/scorep-install

./run.sh
#or:
./run-flink.sh
```

## Running the scripts on Taurus

An example SLURM batch script can be found in taurus-example. It is configurable via the variable EVENTS_DIR in run.sbatch.

Alternatively, you can use the files from the directory `flink-taurus-example`. The workflow is as follows:

1. Copy `flink-taurus-example` to Taurus.
2. Copy event data files from your Flink application (see configuration variable `jobmanager.archive.fs.dir`) into the subdirectory `flink-conf-template/events` of `flink-taurus-example`.
3. Run `sbatch run.sbatch`.
4. When the job finished, you find the traces in `$HOME/cluster-conf-$JOB_ID/flink/traces`, where `$JOB_ID` stands for the job that you started with the sbatch command.
5. You can then open files `traces.otf2` using Vampir.

## Making the generated trace easier to understand

The project at https://gitlab.hrz.tu-chemnitz.de/scads.ai/performance-investigations/big-data-vsettings-file-creator can be used to provide colors and filters for the Vampir visualization of the traces. It is automatically used if you start the scripts as following:

```bash
OUTPUT_DIR=$PWD
SCOREP_INSTALL=$PWD/scorep-install
VSETTINGS_GENERATOR_DIR=$PWD/big-data-vsettings-file-creator

./run.sh
#or:
./run-flink.sh
```

## Test environments

The scripts were last tested with the following versions, but other versions might work as well:

- Spark version: spark-3.0.1-bin-hadoop2.7
- Flink version: flink-1.14.2
- Score-P branch: java (commit: 7c3737882d4c4d646dbe2b247425921163221af1)

## References

If you like this project, we would be happy if you cite the following publication:

```
FRENZEL, Jan; JÄKEL, René. Job Performance Overview of Apache Flink and Apache Spark Applications. Research Poster at The International Conference for High Performance Computing, Networking, Storage, and Analysis (SC19), 2019.
```

You can find links to the poster and poster summary at https://sc19.supercomputing.org/proceedings/tech_poster/tech_poster_pages/rpost205.html.

