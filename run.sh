#!/bin/bash

OUTPUT_DIR=${OUTPUT_DIR:-$PWD}
SCOREP_INSTALL=${SCOREP_INSTALL:-$PWD/scorep-install}
BASE_URL=${BASE_URL:-"http://localhost:8080"}
VSETTINGS_GENERATOR_DIR=${VSETTINGS_GENERATOR_DIR:-../big-data-vsettings-file-creator}

function fail(){
echo $1 >&2
exit
}

if [ ! -d "$SCOREP_INSTALL" ]; then
fail "SCOREP_INSTALL not set properly"
fi

#start the Spark history server if possible
if type -t start-history-server.sh >/dev/null; then
start-history-server.sh
else
echo "No script to start history server. Assuming it is started at http://localhost:8080"
fi

if [ ! -d "$OUTPUT_DIR" ]; then
echo "Creating $OUTPUT_DIR"
mkdir "$OUTPUT_DIR"
fi
echo "Generating programs to $OUTPUT_DIR"

#read stages and their timestamps from history server and create a trace
python3 history-spark.py --output-folder="$OUTPUT_DIR" --scorep-install-path="$SCOREP_INSTALL" --base-url="$BASE_URL" --only-latest

if [ -d "${VSETTINGS_GENERATOR_DIR}" ]; then
	cd "${VSETTINGS_GENERATOR_DIR}"
	find ${OUTPUT_DIR} -name traces.otf2 -exec make run-writer TRACE_PATH={} \;
	cd -
fi

#stop the Spark history server
if type -t stop-history-server.sh >/dev/null; then
stop-history-server.sh
else
echo "No script to stop history server. Leaving it as it is."
fi
