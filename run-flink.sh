#!/bin/bash

OUTPUT_DIR=${OUTPUT_DIR:-$PWD}
SCOREP_INSTALL=${SCOREP_INSTALL:-$PWD/scorep-install}
BASE_URL=${BASE_URL:-"http://localhost:8082"}
VSETTINGS_GENERATOR_DIR=${VSETTINGS_GENERATOR_DIR:-../big-data-vsettings-file-creator}

function fail(){
echo $1 >&2
exit
}

if [ ! -d "$SCOREP_INSTALL" ]; then
fail "SCOREP_INSTALL not set properly"
fi

if [ -n "$FLINK_ROOT_DIR" ]; then
cd $FLINK_ROOT_DIR/bin
. config.sh
cd -
fi
HOSTNAME=`hostname`

#start the Flink history server if possible
if type -t historyserver.sh >/dev/null; then
historyserver.sh start &
sleep 1
while ! grep "Web frontend listening" $FLINK_LOG_DIR/flink-$USER-historyserver-0-$HOSTNAME.log; do
sleep 1
done
else
echo "No script to start history server. Assuming it is started at $BASE_URL"
fi

if [ ! -d "$OUTPUT_DIR" ]; then
echo "Creating $OUTPUT_DIR"
mkdir "$OUTPUT_DIR"
fi
echo "Generating programs to $OUTPUT_DIR"

#read stages and their timestamps from history server and create a trace
python3 history-flink.py --output-folder="$OUTPUT_DIR" --scorep-install-path="$SCOREP_INSTALL" --base-url="$BASE_URL" --only-latest

if [ -d "${VSETTINGS_GENERATOR_DIR}" ]; then
	cd "${VSETTINGS_GENERATOR_DIR}"
	find ${OUTPUT_DIR} -name traces.otf2 -exec make run-writer-flink TRACE_PATH={} \;
	cd -
fi

#stop the Flink history server
if type -t historyserver.sh >/dev/null; then
historyserver.sh stop
else
echo "No script to stop history server. Leaving it as it is."
fi
