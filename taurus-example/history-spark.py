#!/usr/bin/env python
import sys
import requests
from datetime import datetime
epoque = datetime.utcfromtimestamp(0)
baseUrlSuffix = "/api/v1/applications"

def toMillies(timestr):
    return int((datetime.strptime(timestr[:-3],"%Y-%m-%dT%H:%M:%S.%f")-epoque).total_seconds()*1000)

def getFileName(basePath, appId):
    return basePath+"/scorep-example-"+appId+".c"

def generatePrefixText(basePath, appId):
    return """#define NOCROSS_BUILD
#define BACKEND_BUILD_NOMPI
#include <config.h>
#include <scorep/SCOREP_PublicTypes.h>
#include <scorep/SCOREP_User_Types.h>
#include <config-common.h>
#include <scorep_location_management.h>
#include <SCOREP_Events.h>
#include <SCOREP_RuntimeManagement.h>
#include <SCOREP_Definitions.h>
#include <scorep_subsystem_management.h>
#include <SCOREP_Thread_Mgmt.h>
#include <stdint.h>
uint64_t g_startTime;
uint64_t g_endTime;
typedef struct{
char *name;
uint64_t start;
uint64_t end;
} region_t;
region_t regions[]={"""

def generateSuffixText():
	return """};
uint64_t SCOREP_Timer_GetClockResolution(void){
        return UINT64_C(1000);
}
void SCOREP_GetGlobalEpoch(uint64_t* globalEpochBegin, uint64_t* globalEpochEnd){
        *globalEpochBegin = g_startTime;
        *globalEpochEnd = g_endTime;
}
uint64_t SCOREP_GetBeginEpoch(void){
        return g_startTime;
}
uint64_t SCOREP_GetEndEpoch(void){
        return g_endTime;
}
void createSection(const char *name, uint64_t starttime, uint64_t endtime){
        SCOREP_Location *location = SCOREP_Location_CreateCPULocation( name );
        scorep_subsystems_initialize_location(location, NULL);
        SCOREP_Thread_ActivateLocation( location, NULL);
        SCOREP_RegionHandle regionHandle = SCOREP_Definitions_NewRegion( name, NULL, SCOREP_INVALID_SOURCE_FILE, SCOREP_INVALID_LINE_NO, SCOREP_INVALID_LINE_NO, SCOREP_PARADIGM_USER, SCOREP_USER_REGION_TYPE_COMMON );
        SCOREP_Location_EnterRegionUnchecked( location, starttime, regionHandle);
        SCOREP_Location_ExitRegionUnchecked( location, endtime, regionHandle);
}
int main(int argc, char ** argv){
        int regionCount = (sizeof(regions)/sizeof(region_t));
        g_startTime = regions[0].start;
        uint64_t currentMax = 0;
        SCOREP_InitMeasurement();
        for(int i = 0; i < regionCount; i++){
            currentMax = (currentMax > regions[i].end)? currentMax : regions[i].end;
            createSection(regions[i].name, regions[i].start, regions[i].end);
        }
        g_endTime = currentMax;
        SCOREP_FinalizeMeasurement();
        return 0;
}"""

def generateData(baseUrl):
    response = requests.get(baseUrl + "/stages")
    if response.ok:
        stages = response.json()
        sortedStages = sorted(stages, key=lambda stage: stage["stageId"])
        sections = []
        for stage in sortedStages:
            stageId = str(stage["stageId"])
            numTasks = str(stage["numTasks"])
            sections.append("{\"Stage "+stageId+": "+stage["name"]+"\", "+str(toMillies(stage["submissionTime"]))+", "+str(toMillies(stage["completionTime"]))+"}")
            taskResponse = requests.get(baseUrl + "/stages/" + stageId + "/0/taskList?length="+numTasks)
            if taskResponse.ok:
                tasks = taskResponse.json();
                sortedTasks = sorted(tasks, key=lambda task: task["taskId"])
                for task in sortedTasks:
                    dmillies = toMillies(task["launchTime"])
                    sections.append("{\"Stage "+stageId+": Task "+str(task["taskId"])+"\", "+str(dmillies)+", "+str(dmillies+task["duration"])+"}")
    return ",\n".join(sections)

def getAppsSorted(baseUrl):
    response = requests.get(baseUrl)
    appIds = []
    if response.ok:
        apps = response.json()
        sortedApps = sorted(apps, key=lambda app: app["attempts"][0]["endTime"], reverse=True)
        for app in sortedApps:
            appIds.append(app["id"])
    return appIds

def getLatestApp(baseUrl):
    apps = getAppsSorted(baseUrl)
    if len(apps) > 0:
        return apps[0]
    return None

def writeCFile(basePath, appId, data):
    with open(getFileName(basePath, appId), "w") as f:
        f.write(data)

def generateCFile(basePath, appId, baseUrl):
    data = "\n".join([generatePrefixText(basePath, appId),generateData(baseUrl+"/"+appId),generateSuffixText()])
    writeCFile(basePath, appId, data)

def generateShellScript(scorepinstallpath, scorepsourcepath, basePath, apps):
    compileStrings = ["""#!/usr/bin/env bash
export SCOREP_WRAPPER_INSTRUMENTER_FLAGS=--nocompiler
export BASE_SCOREP="""+scorepsourcepath+"""
export SCOREP_INSTALL="""+scorepinstallpath+"""
CFLAGS="-DPACKAGE_ERROR_CODES_HEADER=<SCOREP_ErrorCodes.h> -DAFS_PACKAGE_SRCDIR=\\".\\" -DAFS_PACKAGE_NAME=SCOREP -I$BASE_SCOREP/src -I$BASE_SCOREP/src/measurement -I$BASE_SCOREP/src/measurement/include -I$BASE_SCOREP/include -I$BASE_SCOREP/src/measurement/definitions/include -I$BASE_SCOREP/src/utils/include -I$BASE_SCOREP/src/measurement/include -I$BASE_SCOREP/src/measurement/definitions/include -I$BASE_SCOREP/common/utils/include -I$BASE_SCOREP/src/measurement/thread/include_extern -I$BASE_SCOREP/build-config/common -I. -L$SCOREP_INSTALL/lib -lscorep_measurement -lscorep_adapter_java_user_mgmt"
export SCOREP_TIMER=gettimeofday
export SCOREP_TOTAL_MEMORY=3g
export SCOREP_ENABLE_TRACING=1

touch config-backend.h
sed 's#@AFS_PACKAGE_NAME@#SCOREP#g;s#@.*@##g;' $BASE_SCOREP/common/utils/src/exception/ErrorCodes.tmpl.h > SCOREP_ErrorCodes.h

function create_trace(){
app=$1
$SCOREP_INSTALL/bin/scorep-gcc scorep-example-${app}.c -oscorep-example-${app} $CFLAGS
export SCOREP_EXPERIMENT_DIRECTORY=scorep-${app}
./scorep-example-${app}
cat <<EOF > scorep-${app}/function-groups.def
BEGIN_OPTIONS
        MATCHING_STRATEGY=FIRST
        CASE_SENSITIVE_FUNCTION_NAME=NO
        CASE_SENSITIVE_MANGLED_NAME=NO
        CASE_SENSITIVE_SOURCE_FILE_NAME=NO
END_OPTIONS

BEGIN_FUNCTION_GROUP Task
        NAME=*Task*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP Stage
        NAME=*
END_FUNCTION_GROUP
EOF
}
"""]
    for appId in apps:
        compileStrings.append("create_trace "+appId)
    with open(basePath+"/generateTraces.sh","w") as f:
        f.write("\n".join(compileStrings))

#latestApp = getLatestApp(baseUrl)
#if not latestApp is None:
#    generateCFile(basePath, latestApp, baseUrl)

argc = len(sys.argv)
basepath = "/tmp"
if argc >= 2:
    basepath = str(sys.argv[1])
scorepinstallpath = "/scratch/s2817051/scorep-install-tauruslogin6-modified"
if argc >= 3:
    scorepinstallpath = str(sys.argv[2])
scorepsourcepath = "/scratch/s2817051/SCOREP_JAVA_MODIFIED"
if argc >= 4:
    scorepsourcepath = str(sys.argv[3])
baseUrl = "http://localhost:8080/api/v1/applications"
if argc >= 5:
    baseUrl = str(sys.argv[4])
    baseUrl += baseUrlSuffix
apps = getAppsSorted(baseUrl)
for app in apps:
    generateCFile(basepath, app, baseUrl)

generateShellScript(scorepinstallpath, scorepsourcepath, basepath, apps)
