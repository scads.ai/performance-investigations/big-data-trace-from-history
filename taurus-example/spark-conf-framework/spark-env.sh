#!/usr/bin/env bash

#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This file is sourced when running various Spark programs.
# Copy it as spark-env.sh and edit that to configure Spark for your site.

export SPARK_LOCAL_DIRS=FRAMEWORK_LOCAL_DIR/spark/local
export SPARK_CONF_DIR=FRAMEWORK_CONF_DIR
export SPARK_LOG_DIR=FRAMEWORK_LOG_DIR
export SPARK_PID_DIR=FRAMEWORK_PID_DIR
export JAVA_HOME=FRAMEWORK_JAVA_HOME
