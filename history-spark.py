#!/usr/bin/env python3
import argparse
import pathlib
import os
import sys
import requests
from datetime import datetime
epoque = datetime.utcfromtimestamp(0)
baseUrlSuffix = "/api/v1/applications"

def toMillies(timestr):
    return int((datetime.strptime(timestr[:-3],"%Y-%m-%dT%H:%M:%S.%f")-epoque).total_seconds()*1000)

def getAppsSorted(baseUrl):
    response = requests.get(baseUrl)
    appIds = []
    if response.ok:
        apps = response.json()
        sortedApps = sorted(apps, key=lambda app: app["attempts"][0]["endTime"], reverse=True)
        for app in sortedApps:
            appIds.append(app["id"])
    return appIds

def prepareData(baseUrl, app):
    stagesUrl = baseUrl + "/" + app + "/stages"
    stageList = []
    response = requests.get(stagesUrl)
    if response.ok:
        stages = response.json()
        stages = filter(lambda stage: stage["status"] == "COMPLETE", stages)
        sortedStages = sorted(stages, key=lambda stage: stage["stageId"])
        for stage in sortedStages:
            stageId = str(stage["stageId"])
            stageData = {
                "name": "Stage %s: %s"%(stageId, stage["name"]),
                "startTime": toMillies(stage["submissionTime"]),
                "endTime": toMillies(stage["completionTime"]),
                "inputBytes": stage["inputBytes"],
                "inputRecords": stage["inputRecords"],
                "outputBytes": stage["outputBytes"],
                "outputRecords": stage["outputRecords"],
                "shuffleReadBytes": stage["shuffleReadBytes"],
                "shuffleReadRecords": stage["shuffleReadRecords"],
                "shuffleWriteBytes": stage["shuffleWriteBytes"],
                "shuffleWriteRecords": stage["shuffleWriteRecords"],
                "taskData": []
            }
            taskResponse = requests.get(stagesUrl + "/" + stageId + "?details=true")
            if taskResponse.ok:
                taskList = []
                tasks = taskResponse.json()[0]["tasks"]
                for taskId, task in tasks.items():
                    startTime = toMillies(task["launchTime"])
                    failed = " (failed)" if task["status"] == "FAILED" else ""
                    taskData = {
                        "name": "Stage %s: Task %s%s" % (stageId,str(task["taskId"]), failed),
                        "created": startTime,
                        "scheduled": startTime + task["schedulerDelay"],
                        "deserialization": startTime + task["schedulerDelay"] + task["taskMetrics"]["executorDeserializeTime"],
                        "end": startTime + task["duration"],
                        "bytesRead": task["taskMetrics"]["inputMetrics"]["bytesRead"],
                        "recordsRead": task["taskMetrics"]["inputMetrics"]["recordsRead"],
                        "bytesWritten": task["taskMetrics"]["outputMetrics"]["bytesWritten"],
                        "recordsWritten": task["taskMetrics"]["outputMetrics"]["recordsWritten"],
                        "shuffleRemoteBlocksFetched": task["taskMetrics"]["shuffleReadMetrics"]["remoteBlocksFetched"],
                        "shuffleLocalBlocksFetched": task["taskMetrics"]["shuffleReadMetrics"]["localBlocksFetched"],
                        "shuffleRemoteBytesRead": task["taskMetrics"]["shuffleReadMetrics"]["remoteBytesRead"],
                        "shuffleRemoteBytesReadToDisk": task["taskMetrics"]["shuffleReadMetrics"]["remoteBytesReadToDisk"],
                        "shuffleLocalBytesRead": task["taskMetrics"]["shuffleReadMetrics"]["localBytesRead"],
                        "shuffleRecordsRead": task["taskMetrics"]["shuffleReadMetrics"]["recordsRead"],
                        "shuffleBytesWritten": task["taskMetrics"]["shuffleWriteMetrics"]["bytesWritten"],
                        "shuffleRecordsWritten": task["taskMetrics"]["shuffleWriteMetrics"]["recordsWritten"],
                    }
                    taskList.append(taskData)
                taskList = sorted(taskList, key=lambda item: item["created"])
                stageData["taskData"] = taskList
            stageList.append(stageData)
    return stageList

def createTraceFile(preparedData, appFolder):
    with otf2.writer.open(appFolder, timer_resolution=1000) as trace:

        parent_node = trace.definitions.system_tree_node("node")
        system_tree_node = trace.definitions.system_tree_node("saurus1", parent=parent_node)
        if len(preparedData) > 0:
            inputAttributeNames = list(filter(lambda x: "Read" in x or "input" in x, preparedData[0].keys()))
            outputAttributeNames = list(filter(lambda x: "Write" in x or "output" in x, preparedData[0].keys()))
            inputAttributeDefs = list(map(lambda x: trace.definitions.attribute(x, x, Type.UINT64), inputAttributeNames))
            outputAttributeDefs = list(map(lambda x: trace.definitions.attribute(x, x, Type.UINT64), outputAttributeNames))
            inputTaskAttributeNames = list(filter(lambda x: "Read" in x or "Fetched" in x, preparedData[0]["taskData"][0].keys()))
            outputTaskAttributeNames = list(filter(lambda x: "Written" in x, preparedData[0]["taskData"][0].keys()))
            inputTaskAttributeDefs = list(map(lambda x: trace.definitions.attribute(x, x, Type.UINT64), inputTaskAttributeNames))
            outputTaskAttributeDefs = list(map(lambda x: trace.definitions.attribute(x, x, Type.UINT64), outputTaskAttributeNames))

            for stage in preparedData:
                location_group = trace.definitions.location_group(stage["name"], system_tree_parent=system_tree_node)
                writer = trace.event_writer(stage["name"], group=location_group)
                function = trace.definitions.region("Stage")
                inputAttributes = {inputAttributeDefs[i]: stage[inputAttributeNames[i]] for i in range(len(inputAttributeNames))}
                outputAttributes = {outputAttributeDefs[i]: stage[outputAttributeNames[i]] for i in range(len(outputAttributeNames))}
                writer.enter(stage["startTime"], function, inputAttributes)
                writer.leave(stage["endTime"], function, outputAttributes)
                for task in stage["taskData"]:
                    writer = trace.event_writer(task["name"], group=location_group)
                    timeIndexes = ["created", "scheduled", "deserialization", "end"]
                    timeSpans = ["SCHEDULER_DELAY", "TASK_DESERIALIZATION", "RUNNING"]
                    for i in range(len(timeSpans)):
                        function = trace.definitions.region(timeSpans[i])
                        if "RUNNING" == timeSpans[i]:
                            inputTaskAttributes = {inputTaskAttributeDefs[i]: task[inputTaskAttributeNames[i]] for i in range(len(inputTaskAttributeNames))}
                            outputTaskAttributes = {outputTaskAttributeDefs[i]: task[outputTaskAttributeNames[i]] for i in range(len(outputTaskAttributeNames))}
                            writer.enter(task[timeIndexes[i]], function, inputTaskAttributes)
                            writer.leave(task[timeIndexes[i + 1]], function, outputTaskAttributes)
                        else:
                            writer.enter(task[timeIndexes[i]], function)
                            writer.leave(task[timeIndexes[i + 1]], function)

def createFunctionGroupFile(outputFolder):
    path=pathlib.Path(outputFolder) / "function-groups.def"
    with path.open(mode="w") as f:
        f.write("""BEGIN_OPTIONS
    MATCHING_STRATEGY=FIRST
    CASE_SENSITIVE_FUNCTION_NAME=NO
    CASE_SENSITIVE_MANGLED_NAME=NO
    CASE_SENSITIVE_SOURCE_FILE_NAME=NO
END_OPTIONS

BEGIN_FUNCTION_GROUP SCHEDULER_DELAY
    NAME=*SCHEDULER_DELAY*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP TASK_DESERIALIZATION
    NAME=*TASK_DESERIALIZATION*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP RUNNING
    NAME=*RUNNING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP Stage
    NAME=*
END_FUNCTION_GROUP
""")

parser = argparse.ArgumentParser(description="Create trace from job history")
parser.add_argument('--output-folder', dest="outputFolder", type=pathlib.Path, default=os.environ["HOME"]+"/log-data-trace")
parser.add_argument('--scorep-install-path', dest="scorepInstallPath", type=pathlib.Path, default="/scratch/ws/1/s2817051-easybuild-test/software/Score-P/8.0-dev-java-gompic-2020b")
parser.add_argument('--base-url', dest="baseUrl", default="http://localhost:8080")
parser.add_argument('--only-latest', dest="latestOnly", action="store_true")
parser.set_defaults(latestOnly=False)

args = parser.parse_args()
baseUrl = args.baseUrl + baseUrlSuffix
#before importing otf2, the path to it has to be configured
#take the first path matching <scorep_install_path>/lib/python*/site-packages:
sys.path.append(str(next(args.scorepInstallPath.glob("lib/python*/site-packages"))))
import otf2
from otf2.enums import Type
outputFolder = args.outputFolder
if not os.path.exists(outputFolder):
    os.makedirs(outputFolder)
apps = getAppsSorted(baseUrl)
if args.latestOnly:
    if len(apps) > 0:
        apps = [apps[0]]
    else:
        apps = []
for app in apps:
    appFolder = outputFolder / app
    preparedData = prepareData(baseUrl, app)
    createTraceFile(preparedData, str(appFolder))
    createFunctionGroupFile(appFolder)
