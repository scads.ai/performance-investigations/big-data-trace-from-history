#!/bin/bash

OUTPUT_DIR=${OUTPUT_DIR:-$PWD}
SCOREP_INSTALL=${SCOREP_INSTALL:-$PWD/scorep-install}
SCOREP_SOURCE=${SCOREP_SOURCE:-$PWD/TRY_SCADS_java}
BASE_URL=${BASE_URL:-"http://localhost:8082"}

function fail(){
echo $1 >&2
exit
}

if [ ! -d "$SCOREP_INSTALL" ]; then
fail "SCOREP_INSTALL not set properly"
fi

if [ ! -d "$SCOREP_SOURCE" ]; then
fail "SCOREP_SOURCE not set properly"
fi

cd $FLINK_ROOT_DIR/bin
. config.sh
cd -
HOSTNAME=`hostname`

#start the Flink history server if possible
if type -t historyserver.sh >/dev/null; then
historyserver.sh start &
sleep 1
while ! grep "Web frontend listening" $FLINK_LOG_DIR/flink-$USER-historyserver-0-$HOSTNAME.log; do
sleep 1
done
else
echo "No script to start history server. Assuming it is started at $BASE_URL"
fi

if [ ! -d "$OUTPUT_DIR" ]; then
echo "Creating $OUTPUT_DIR"
mkdir "$OUTPUT_DIR"
fi
echo "Generating programs to $OUTPUT_DIR"

#read stages and their timestamps from history server and create C program source files (and the bash script to compile and run them)
python3 history-flink.py "$OUTPUT_DIR" "$SCOREP_INSTALL" "$SCOREP_SOURCE" "$BASE_URL"

cd $OUTPUT_DIR
#compile and run the fake Score-P applications to create the traces
chmod u+x generateTraces.sh
./generateTraces.sh

#stop the Flink history server
if type -t historyserver.sh >/dev/null; then
historyserver.sh stop
else
echo "No script to stop history server. Leaving it as it is."
fi
