#!/usr/bin/env python
import sys
import requests
from datetime import datetime
epoque = datetime.utcfromtimestamp(0)
baseUrlSuffix = "/jobs"

def toMillies(timestr):
    return int(timestr)

def getFileName(basePath, appId):
    return basePath+"/scorep-example-"+appId+".c"

def generatePrefixText(basePath, appId):
    return """#define NOCROSS_BUILD
#define BACKEND_BUILD_NOMPI
#include <config.h>
#include <scorep/SCOREP_PublicTypes.h>
#include <scorep/SCOREP_User_Types.h>
#include <config-common.h>
#include <scorep_location_management.h>
#include <SCOREP_Events.h>
#include <SCOREP_RuntimeManagement.h>
#include <SCOREP_Definitions.h>
#include <scorep_subsystem_management.h>
#include <SCOREP_Thread_Mgmt.h>
#include <stdint.h>
typedef struct{
uint64_t timestamp;
char *name;
} subregion_t;
typedef struct{
char *name;
int regionCount;
subregion_t *subregions;
} region_t;
"""

def generateSuffixText():
	return """};
uint64_t SCOREP_Timer_GetClockResolution(void){
        return UINT64_C(1000);
}
void SCOREP_GetGlobalEpoch(uint64_t* globalEpochBegin, uint64_t* globalEpochEnd){
        *globalEpochBegin = g_startTime;
        *globalEpochEnd = g_endTime;
}
uint64_t SCOREP_GetBeginEpoch(void){
        return g_startTime;
}
uint64_t SCOREP_GetEndEpoch(void){
        return g_endTime;
}
void createSection(const char *name, int regionCount, subregion_t *subregions){
        SCOREP_Location *location = SCOREP_Location_CreateCPULocation( name );
        scorep_subsystems_initialize_location(location, NULL);
        SCOREP_Thread_ActivateLocation( location, NULL);
        for(int i = 0; i < regionCount; i++){
            subregion_t subregion = subregions[i];
            SCOREP_RegionHandle regionHandle = SCOREP_Definitions_NewRegion( subregions[i].name, NULL, SCOREP_INVALID_SOURCE_FILE, SCOREP_INVALID_LINE_NO, SCOREP_INVALID_LINE_NO, SCOREP_PARADIGM_USER, SCOREP_USER_REGION_TYPE_COMMON );
            SCOREP_Location_EnterRegionUnchecked( location, subregions[i].timestamp, regionHandle);
            SCOREP_Location_ExitRegionUnchecked( location, subregions[i + 1].timestamp, regionHandle);
        }
}
int main(int argc, char ** argv){
        int regionCount = (sizeof(regions)/sizeof(region_t));
        SCOREP_InitMeasurement();
        for(int i = 0; i < regionCount; i++){
            createSection(regions[i].name, regions[i].regionCount, regions[i].subregions);
        }
        SCOREP_FinalizeMeasurement();
        return 0;
}"""

def generateData(baseUrl):
    response = requests.get(baseUrl)
    if response.ok:
        vertices = response.json()["vertices"]
        sortedVertices = sorted(vertices, key=lambda vertice: vertice["start-time"])
        sections = ["region_t regions[]={"]
        subsections = []
        taskId = 0;
        currentMinTime = 2**64
        currentMaxTime = 0
        for vertice in sortedVertices:
            verticeTaskId = str(taskId)
            verticeId = vertice["id"]
            numTasks = str(vertice["parallelism"])
            startTime = toMillies(vertice["start-time"])
            endTime = toMillies(vertice["end-time"])
            currentMinTime = min(currentMinTime, startTime)
            currentMaxTime = max(currentMaxTime, endTime)
            subsections.append("subregion_t subregions"+str(taskId)+"[]={{"+str(startTime)+", \"Vertice\"},{"+str(endTime)+"}};")
            sections.append("{\"Vertice "+verticeTaskId+": "+vertice["name"]+"\",1,subregions"+str(taskId)+"},")
            taskId += 1;
            taskResponse = requests.get(baseUrl + "/vertices/" + verticeId + "/subtasktimes")
            if taskResponse.ok:
                tasks = taskResponse.json()["subtasks"]
                sortedTasks = sorted(tasks, key=lambda task: task["subtask"])
                for task in sortedTasks:
                    startTime = toMillies(task["timestamps"]["CREATED"])
                    created = str(startTime)
                    scheduled = str(toMillies(task["timestamps"]["SCHEDULED"]))
                    deploying = str(toMillies(task["timestamps"]["DEPLOYING"]))
                    initializing = str(toMillies(task["timestamps"]["INITIALIZING"]))
                    running = str(toMillies(task["timestamps"]["RUNNING"]))
                    endTime = toMillies(task["timestamps"]["FINISHED"])
                    finished = str(endTime)
                    currentMinTime = min(currentMinTime, startTime)
                    currentMaxTime = max(currentMaxTime, endTime)
                    subsections.append("subregion_t subregions"+str(taskId)+"[]={{"+created+",\"CREATED\"},{"+scheduled+",\"SCHEDULED\"},{"+deploying+",\"DEPLOYING\"},{"+initializing+",\"INITIALIZING\"},{"+running+",\"RUNNING\"},{"+finished+",NULL}};")
                    sections.append("{\"Vertice "+verticeTaskId+": Subtask "+str(task["subtask"])+"\",5,subregions"+str(taskId)+"},")
                    taskId += 1;
    return "uint64_t g_startTime ="+str(currentMinTime)+";\nuint64_t g_endTime ="+str(currentMaxTime)+";\n"+("\n".join(subsections))+("\n".join(sections))[0:-1]

def getAppsSorted(baseUrl):
    response = requests.get(baseUrl+"/overview")
    appIds = []
    if response.ok:
        apps = response.json()["jobs"]
        sortedApps = sorted(apps, key=lambda app: app["end-time"], reverse=True)
        for app in sortedApps:
            appIds.append(app["jid"])
    return appIds

def getLatestApp(baseUrl):
    apps = getAppsSorted(baseUrl)
    if len(apps) > 0:
        return apps[0]
    return None

def writeCFile(basePath, appId, data):
    with open(getFileName(basePath, appId), "w") as f:
        f.write(data)

def generateCFile(basePath, appId, baseUrl):
    data = "\n".join([generatePrefixText(basePath, appId),generateData(baseUrl+"/"+appId),generateSuffixText()])
    writeCFile(basePath, appId, data)

def generateShellScript(scorepinstallpath, scorepsourcepath, basePath, apps):
    compileStrings = ["""#!/usr/bin/env bash
export SCOREP_WRAPPER_INSTRUMENTER_FLAGS=--nocompiler
export BASE_SCOREP="""+scorepsourcepath+"""
export SCOREP_INSTALL="""+scorepinstallpath+"""
CFLAGS="-DPACKAGE_ERROR_CODES_HEADER=<SCOREP_ErrorCodes.h> -DAFS_PACKAGE_SRCDIR=\\".\\" -DAFS_PACKAGE_NAME=SCOREP -I$BASE_SCOREP/src -I$BASE_SCOREP/src/measurement -I$BASE_SCOREP/src/measurement/include -I$BASE_SCOREP/include -I$BASE_SCOREP/src/measurement/definitions/include -I$BASE_SCOREP/src/utils/include -I$BASE_SCOREP/src/measurement/include -I$BASE_SCOREP/src/measurement/definitions/include -I$BASE_SCOREP/common/utils/include -I$BASE_SCOREP/src/measurement/thread/include_extern -I$BASE_SCOREP/build-config/common -I. -L$SCOREP_INSTALL/lib -lscorep_measurement -lscorep_adapter_java_user_mgmt"
export SCOREP_TIMER=gettimeofday
export SCOREP_TOTAL_MEMORY=3g
export SCOREP_ENABLE_TRACING=1

touch config-backend.h
sed 's#@AFS_PACKAGE_NAME@#SCOREP#g;s#@.*@##g;' $BASE_SCOREP/common/utils/src/exception/ErrorCodes.tmpl.h > SCOREP_ErrorCodes.h

function create_trace(){
app=$1
$SCOREP_INSTALL/bin/scorep-gcc scorep-example-${app}.c -oscorep-example-${app} $CFLAGS
export SCOREP_EXPERIMENT_DIRECTORY=scorep-${app}
./scorep-example-${app}
cat <<EOF > scorep-${app}/function-groups.def
BEGIN_OPTIONS
    MATCHING_STRATEGY=FIRST
    CASE_SENSITIVE_FUNCTION_NAME=NO
    CASE_SENSITIVE_MANGLED_NAME=NO
    CASE_SENSITIVE_SOURCE_FILE_NAME=NO
END_OPTIONS

BEGIN_FUNCTION_GROUP CREATED
    NAME=*CREATED*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP SCHEDULED
    NAME=*SCHEDULED*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP DEPLOYING
    NAME=*DEPLOYING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP INITIALIZING
    NAME=*INITIALIZING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP RUNNING
    NAME=*RUNNING*
END_FUNCTION_GROUP

BEGIN_FUNCTION_GROUP Vertice
    NAME=*
END_FUNCTION_GROUP
EOF
}
"""]
    for appId in apps:
        compileStrings.append("create_trace "+appId)
    with open(basePath+"/generateTraces.sh","w") as f:
        f.write("\n".join(compileStrings))

#latestApp = getLatestApp(baseUrl)
#if not latestApp is None:
#    generateCFile(basePath, latestApp, baseUrl)

argc = len(sys.argv)
basepath = "/tmp"
if argc >= 2:
    basepath = str(sys.argv[1])
scorepinstallpath = "/scratch/s2817051/scorep-install-tauruslogin6-modified"
if argc >= 3:
    scorepinstallpath = str(sys.argv[2])
scorepsourcepath = "/scratch/s2817051/SCOREP_JAVA_MODIFIED"
if argc >= 4:
    scorepsourcepath = str(sys.argv[3])
baseUrl = "http://localhost:8080/jobs"
if argc >= 5:
    baseUrl = str(sys.argv[4])
    baseUrl += baseUrlSuffix
apps = getAppsSorted(baseUrl)
for app in apps:
    generateCFile(basepath, app, baseUrl)

generateShellScript(scorepinstallpath, scorepsourcepath, basepath, apps)
